# first-boot
## v0.1

first-boot is intended as fire and forget all-in-one configuration script to harden and prepare a host to be a production environment

### initial implementation
- install required packages
- configure user accounts and user groups
- configure firewall and access permissions
- provided an access keypair
- patch security vulnerabilities
- internal build tests

### pending implementations
- automate installation
- automate backups via API
- deliver keypair to email address
- install convenience scripts
- install maintenance and security cronjobs

### eventual implementations
- host version control
- hosting provider control
- security auditing
- scheduled security auditing
- installation options (different environment configurations)
