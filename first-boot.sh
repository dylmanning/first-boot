#!/usr/bin/env bash

#  first-boot.sh - v0.1
#
#  instance configuration script designed to install all dependancies and applications
#  for a web application environment. designed to be a completely autonomous fire and forget script.
#
#  Dylan Manning
#  dyl.manning@gmail.com

readonly NC="$(echo -e "\033[0m")"
readonly RED="$(echo -e "\033[0;31m")"
readonly BLUE="$(echo -e "\033[0;34m")"
readonly CYAN="$(echo -e "\033[0;36m")"
readonly GREEN="$(echo -e "\033[0;32m")"

readonly VERSION=0.1

services=( sshd ufw fail2ban apparmor nodejs nginx mongodb sendmail git )
dependancies=( build-essential whois apt-show-versions git bc nmap ufw fail2ban nginx mongodb htop apparmor apparmor-profiles rkhunter chkrootkit )

install () {

  # ensure user is root
  if (( $EUID != 0 )); then
    printf "\n\t${RED}%s${NC}\n\n" "You must be root to run this script."
    exit
  fi

  printf "${GREEN}%s${NC}\n" "first-boot - $VERSION"

  # create admin group
  groupadd admin

  # TODO replace for absentee installation
  printf "${CYAN}%s${NC}" "enter local timezone (continent/city): "
  read -r timezone

  printf "${CYAN}%s${NC}" "enter hostname (example.domain.com): "
  read -r hostname

  printf "${CYAN}%s${NC}" "enter domain (example.com): "
  read -r application

  printf "${CYAN}%s${NC}" "enter ssh port (1000 - 1020): "
  read -r port

  # user configuration
  printf "${CYAN}%s${NC}" "enter username: "
  read -r username

  printf "${CYAN}%s${NC}" "enter password: "
  read -r pass

  # printf "${GREEN}%s${NC}" "enter your full name: "
  # read full_name
  #
  # printf "${GREEN}%s${NC}" "enter git username: "
  # read git_user
  #
  # printf "${GREEN}%s${NC}" "enter git email: "
  # read git_email

  # install localepurge first to remove locales through installation
  apt-get install -y localepurge

  for i in "${dependancies[@]}"; do
    printf "\r%s Processing: %s" "$(tput el)" "$i"
    apt-get install -y $i > /dev/null
  done

  egrep "^$username" /etc/passwd > /dev/null
  if [ $? -eq 0 ]; then
    printf "${RED}%s${NC} %s" "$username" "already exists."
    # TODO back to user input
    exit
  fi

  printf "\r%s Processing: %s" "$(tput el)" "user $username"

  useradd -p $(mkpasswd "$password") -d /home/"$username" -m -s /bin/bash "$username"
  usermod -a -G admin "$username"

  printf "\r%s Processing: %s" "$(tput el)" "host and user configurations"

  # remove banners
  rm -rf /etc/update-motd.d/*

  # add users to sudoers
  echo -e "$username\tALL=(ALL:ALL) ALL" >> /etc/sudoers

  # restrict access to su to admin group
  dpkg-statoverride --update --add root admin 4750 /bin/su

  # get public IP address
  local ip=$(dig +short myip.opendns.com @resolver1.opendns.com)

  # set machine hostname
  echo "$hostname" > /etc/hostname
  hostname -F /etc/hostname

  # set public ip and localhost to domain in /etc/hosts
  echo -e "\n$ip $hostname.$application $hostname\n" >> /etc/hosts
  echo -e "127.0.0.1 $hostname.$application $hostname" >> /etc/hosts
  echo -e "::1       $hostname.$application $hostname" >> /etc/hosts

  printf "\r%s Processing: %s" "$(tput el)" "timezone $timezone"

  # set timezone
  timedatectl set-timezone $timezone

  # configure git
  # git config --global user.name "$full_name"
  # git config --global user.email "$git_email"

  printf "\r%s Processing: %s" "$(tput el)" "shh configuration"

  # ssh configuration
  cat ./etc/ssh/sshd_config > /etc/ssh/sshd_config
  sed -i "s/PORT/$port/g" /etc/ssh/sshd_config
  systemctl restart sshd

  printf "\r%s Processing: %s" "$(tput el)" "firewall"

  # ufw configuration
  ufw default deny incoming > /dev/null
  ufw default allow outgoing > /dev/null
  ufw allow $port > /dev/null
  ufw allow http > /dev/null
  ufw allow https > /dev/null
  ufw --force enable > /dev/null

  printf "\r%s Processing: %s" "$(tput el)" "fail2ban"

  # fail2ban configuration
  cp -a ./etc/fail2ban/filter.d/. /etc/fail2ban/filter.d/
  cat ./etc/fail2ban/jail.local > /etc/fail2ban/jail.local
  sed -i "s/PORT/$port/g" /etc/fail2ban/jail.local
  systemctl restart fail2ban

  printf "\n\r%s" "patching security vulnerabilities"

  # secure shared memory
  echo "tmpfs           /run/shm        tmpfs   defaults,noexec,nosuid   0       0" >> /etc/fstab

  # prevent source routing and log malformed IP
  cat ./etc/sysctl.conf >> /etc/sysctl.conf

  # prevent IP spoofing
  echo "nospoof on" >> /etc/host.conf

  printf "\r%s Processing: %s" "$(tput el)" "node"

  # node.js install
  curl -sL https://deb.nodesource.com/setup -o nodesource_setup.sh > /dev/null
  bash nodesource_setup.sh > /dev/null
  apt-get install -y nodejs > /dev/null
  rm nodesource_setup.sh

  printf "\r%s Processing: %s" "$(tput el)" "letsencrypt ssl certificate"

  # letsencrypt install
  # TODO see if can be completed without prompts
  git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt
  systemctl stop nginx
  bash /opt/letsencrypt/letsencrypt-auto certonly --standalone

  printf "\r%s Processing: %s" "$(tput el)" "nginx webserver"

  # copy nginx conf and https config
  cat ./etc/nginx/sites-available/default > /etc/nginx/sites-available/default
  sed -i "s/HOST/$application/g" /etc/nginx/sites-available/default
  systemctl start nginx

  printf "\r%s Processing: %s" "$(tput el)" "mongoDB database"

  # mongoDB directory
  mkdir /data
  mkdir /data/db
  mongod --fork --logpath /var/log/mongodb.log

  printf "\r%s Processing: %s" "$(tput el)" "udpdating"

  apt-get update > /dev/null && apt-get upgrade -y > /dev/null && apt-get autoclean > /dev/null

  # generate key
  mkdir /home/"$username"/.ssh
  chown dyl:admin /home/"$username"/.ssh
  chmod 700 /home/"$username"/.ssh
  cd /home/"$username"/.ssh
  sudo -H -u $username ssh-keygen -t rsa -N "" -b 4096 -f key -q
  sudo cat key.pub >> .ssh/authorized_keys
  chown dyl:admin authorized_keys
  chmod 600 authorized_keys
  sudo -H -u $username openssl rsa -in key -outform pem > pk_key.pem
  sudo -H -u $username openssl rsa -in pk_key.pem -pubout -out pub_key.pem
  printf "\n${RED}%s${NC}\n" "IMPORTANT - copy the contents of this file to your local machine to regain access to this machine"
  cat pub_key.pem
  cd /

  # clean up and exit
  # cat /dev/null > ~/.bash_history && history -c && sudo reboot

  echo -e "\n done test"

}

# install security
# https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-14-04
# https://www.digitalocean.com/community/tutorials/how-to-protect-an-nginx-server-with-fail2ban-on-ubuntu-14-04
# https://www.thefanclub.co.za/how-to/how-secure-ubuntu-1604-lts-server-part-1-basics

# install webserver
# https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04

# install web application for this machine
install_application () {

  # clone application
  rm -rf /var/www/*
  clone=git@gitlab.com:$git_user/$application.git
  git clone $clone /var/www/

  # assign application to admin user
  chown /var/www/"$application $user"

  # install pm2
  npm install -g pm2

  cd /var/www/
  pm2 startup ubuntu
  # do startup command
  # fork app maximum cpu
  pm2 save

}

# status check
check () {

  # check dependancies
  printf "\ncheck.packages \n"
  printf "%-20s %-20s\n" "application" "status"
  printf "%40s\n" | tr ' ' -

  for i in "${dependancies[@]}"; do
    installed=1

    dpkg-query -l $i > /dev/null || installed=0

    if [ $installed -eq 1 ]; then
      status="${GREEN}installed${NC}"
    else
      status="${RED}missing${NC}"
    fi

    printf "%-20s %-20s\n" $i $status
  done

  # check serivces
  printf "\ncheck.services \n"
  printf "%-20s %-10s %-10s\n" "service" "status" "activity"
  printf "%40s\n" | tr ' ' -

  for i in "${services[@]}"; do
    enabled=1
    active=1

    systemctl is-enabled $i > /dev/null || enabled=0
    systemctl is-active $i > /dev/null || active=0

    if [ $enabled -eq 1 ]; then
      is_enabled="${GREEN}enabled${NC}"
    else
      is_enabled="${RED}disabled${NC}"
    fi

    if [ $active -eq 1 ]; then
      is_active="${GREEN}running${NC}"
    else
      is_active="${RED}inactive${NC}"
    fi

    printf "%-20s %-20s %-10s\n" $i $is_enabled $is_active
  done

}

# cronjobs
#function install_cron {

    # vultr api
    # backup
    # logwatch
    # letencrypt auto renewal

#}

# implement convient scripts and aliases
#function install_scripts {

    # vultr api
    # update
    # gitpull
    # backup

#}

# motd and banners
#function install_banners {

    # ssh banner
    # motd banner

#}
